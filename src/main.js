import Vue from 'vue'
import App from './App.vue'
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar)

Vue.config.productionTip = false

const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '500px',
  transition: {
    speed: '0.02s',
    opacity: '0.60s',
    termination: 300
  },
  autoRevert: true,
  location: 'left',
  inverse: false
}

Vue.use(VueProgressBar, options)
new Vue({
  render: h => h(App),
}).$mount('#app')
